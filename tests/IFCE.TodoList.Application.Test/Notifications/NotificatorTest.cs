﻿using Bogus;
using Xunit;
using System;
using System.Linq;
using FluentAssertions;
using FluentValidation.Results;
using FluentAssertions.Execution;
using IFCE.TodoList.Application.Notifications;

namespace IFCE.TodoList.Application.Test.Notifications;

public class NotificatorTest
{
    private readonly Faker _faker;
    private readonly Notificator _notificator;

    public NotificatorTest()
    {
        _faker = new Faker("pt_BR");
        _notificator = new Notificator();
    }

    [Fact]
    [Trait("Notificator", "Handle")]
    public void Handle_PassandoNotification_ContemNotificacao()
    {
        // Arrange
        var mensagem = _faker.Random.String();
        var notification = new Notification(mensagem);
        
        // Act
        _notificator.Handle(notification);
        
        // Assert
        using (new AssertionScope())
        {
            _notificator.HasNotification.Should().BeTrue();
            _notificator.IsNotFoundResource.Should().BeFalse();
            _notificator.GetNotifications().Should().Contain(notification);
        }
    }
    
    [Fact]
    public void Handle_PassandoValidationFailure_ContemNotificacao()
    {
        // Arrange
        var faker = new Faker<ValidationFailure>("pt_BR");
        faker
            .CustomInstantiator(f => new ValidationFailure(f.Random.String(), f.Random.String()));

        var validationFailures = faker.Generate(4);

        var mensagens = validationFailures.Select(c => c.ErrorMessage);
        
        // Act
        _notificator.Handle(validationFailures);
        
        // Assert
        using (new AssertionScope())
        {
            _notificator.HasNotification.Should().BeTrue();
            _notificator.IsNotFoundResource.Should().BeFalse();
            _notificator.GetNotifications().Should().HaveCount(4);
            _notificator.GetNotifications().Select(c => c.Message).Should().Equal(mensagens);
        }
    }

    [Fact]
    public void HandleNotFoundResource_ExecutaComSucesso()
    {
        // Act
        _notificator.HandleNotFoundResource();
        
        // Assert
        using (new AssertionScope())
        {
            _notificator.HasNotification.Should().BeFalse();
            _notificator.IsNotFoundResource.Should().BeTrue();
            _notificator.GetNotifications().Should().BeEmpty();
        }
    }

    [Fact]
    public void Handle_AposChamarNotFoundResource_DeveLancarExcecao()
    {
        // Arrange
        var notification = new Notification("Erro");

        // Act
        _notificator.HandleNotFoundResource();
        var action = () => _notificator.Handle(notification);

        // Assert
        action
            .Should()
            .ThrowExactly<InvalidOperationException>()
            .WithMessage("Cannot call Handle when there are NotFoundResouce!");
    }
    
    [Fact]
    public void HandleNotFoundResource_AposChamarHandle_DeveLancarExcecao()
    {
        // Arrange
        var notification = new Notification("Erro");

        // Act
        _notificator.Handle(notification);
        var action = () => _notificator.HandleNotFoundResource();
        
        // Assert
        action
            .Should()
            .ThrowExactly<InvalidOperationException>()
            .WithMessage("Cannot call HandleNotFoundResource when there are notifications!");
    }
}