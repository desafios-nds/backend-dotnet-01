﻿using AutoMapper;
using IFCE.TodoList.Domain.Model;
using IFCE.TodoList.Application.DTO.Assignment;
using IFCE.TodoList.Application.DTO.AssignmentList;
using IFCE.TodoList.Domain.Filter;

namespace IFCE.TodoList.Application.AutoMapper;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<AssignmentSearchDto, AssignmentFilter>().ReverseMap();
        CreateMap<AssignmentDto, Assignment>().ReverseMap();
        CreateMap<AddAssignmentDto, Assignment>().ReverseMap();
        CreateMap<EditAssignmentDto, Assignment>().ReverseMap();
        
        CreateMap<AssignmentListDto, AssignmentList>().ReverseMap();
        CreateMap<AddAssignmentListDto, AssignmentList>().ReverseMap();
        CreateMap<EditAssignmentListDto, AssignmentList>().ReverseMap();
    }
}