﻿using IFCE.TodoList.Application.DTO.Paged;

namespace IFCE.TodoList.Application.DTO.AssignmentList;

public class AssignmentListSearchDto : BaseSearchDto
{
    public string Name { get; set; }
    public string OrderBy { get; set; } = "description";
    public string OrderDir { get; set; } = "asc";
}