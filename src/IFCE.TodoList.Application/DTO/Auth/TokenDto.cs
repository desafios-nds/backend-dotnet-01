﻿using IFCE.TodoList.Application.DTO.User;

namespace IFCE.TodoList.Application.DTO.Auth;

public class TokenDto
{
    public string AccessToken { get; set; }
    public double ExpiresIn { get; set; }
    public UserDto User { get; set; }
}