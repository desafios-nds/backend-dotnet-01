﻿using System.ComponentModel.DataAnnotations;

namespace IFCE.TodoList.Application.DTO.Assignment;

public class AddAssignmentDto
{
    [Required]
    public string Description { get; set; }
    public DateTime? Deadline { get; set; }
    public Guid? AssignmentListId { get; set; } = null;
}