﻿using FluentValidation.Results;

namespace IFCE.TodoList.Application.Notifications;

public interface INotificator
{
    void Handle(Notification notification);
    void Handle(List<ValidationFailure> failures);
    void HandleNotFoundResource();
    IEnumerable<Notification> GetNotifications();
    bool HasNotification { get; }
    bool IsNotFoundResource { get; }
}