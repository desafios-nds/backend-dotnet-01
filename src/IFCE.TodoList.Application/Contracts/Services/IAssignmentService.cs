﻿using IFCE.TodoList.Application.DTO.Assignment;
using IFCE.TodoList.Application.DTO.Paged;

namespace IFCE.TodoList.Application.Contracts.Services;

public interface IAssignmentService
{
    Task<PagedDto<AssignmentDto>> Search(AssignmentSearchDto search);
    Task<AssignmentDto> GetById(Guid id);
    Task<AssignmentDto> Add(AddAssignmentDto assignmentDto);
    Task<AssignmentDto> Edit(Guid id, EditAssignmentDto assignmentDto);
    Task MarkConcluded (Guid id);
    Task MarkNotConcluded (Guid id);
    Task Delete(Guid id);
}