﻿using IFCE.TodoList.Application.DTO.Auth;

namespace IFCE.TodoList.Application.Contracts.Services;

public interface IAuthService
{
    Task<TokenDto> Login(LoginDto user);
    Task<TokenDto> Register(RegisterDto user);
}