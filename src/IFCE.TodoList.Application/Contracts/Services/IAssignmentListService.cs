﻿using IFCE.TodoList.Application.DTO.Assignment;
using IFCE.TodoList.Application.DTO.AssignmentList;
using IFCE.TodoList.Application.DTO.Paged;

namespace IFCE.TodoList.Application.Contracts.Services;

public interface IAssignmentListService
{
    Task<PagedDto<AssignmentListDto>> Search(AssignmentListSearchDto search);
    Task<PagedDto<AssignmentDto>> SearchAssignments(Guid id, AssignmentSearchDto search);
    Task<AssignmentListDto> GetById(Guid id);
    Task<AssignmentListDto> Add(AddAssignmentListDto assignmentListDto);
    Task<AssignmentListDto> Edit(Guid id, EditAssignmentListDto assignmentListDto);
    Task Delete(Guid id);
}