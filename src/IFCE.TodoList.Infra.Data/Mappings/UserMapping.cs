﻿using IFCE.TodoList.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IFCE.TodoList.Infra.Data.Mappings;

public class UsuarioMapping : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder
            .HasKey(c => c.Id);

        builder
            .Property(c => c.Name)
            .IsRequired()
            .HasColumnType("VARCHAR(150)");
        
        builder
            .Property(c => c.Email)
            .IsRequired()
            .HasColumnType("VARCHAR(100)");
        
        builder
            .Property(c => c.Password)
            .IsRequired()
            .HasColumnType("VARCHAR(255)");
        
        builder
            .Property(c => c.CreatedAt)
            .ValueGeneratedOnAdd()
            .HasColumnType("DATETIME");
        
        builder
            .Property(c => c.UpdatedAt)
            .ValueGeneratedOnAddOrUpdate()
            .HasColumnType("DATETIME");

        builder
            .HasMany(c => c.Assignments)
            .WithOne(c => c.User)
            .HasForeignKey(c => c.UserId) // Se seguir o padrão de nomeclatura, essa instrução é opcional
            .OnDelete(DeleteBehavior.Restrict);

        builder
            .HasMany(c => c.AssignmentLists)
            .WithOne(c => c.User)
            .OnDelete(DeleteBehavior.Restrict);
    }
}