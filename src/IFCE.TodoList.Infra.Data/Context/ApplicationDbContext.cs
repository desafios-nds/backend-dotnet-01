﻿using System.Reflection;
using IFCE.TodoList.Domain.Model;
using Microsoft.EntityFrameworkCore;
using IFCE.TodoList.Domain.Contracts;

namespace IFCE.TodoList.Infra.Data.Context;

public class ApplicationDbContext : DbContext, IUnitOfWork
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
    { }

    public DbSet<User> Users { get; set; }
    public DbSet<Assignment> Assignments { get; set; }
    public DbSet<AssignmentList> AssignmentLists { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        
        // Vai aplicar uma configuração padrão para as propriedades que não forem mapeadas.
        // Setando como varchar(100)
        // Deve vir antes do *modelBuilder.ApplyConfigurationsFromAssembly()*
        foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(
                     e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
            property.SetColumnType("varchar(100)");
        
        // Vai aplicar os mappings (Modelo -> Banco) usando reflection.
        // Os mappings ficam na pasta Mappings
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }

    public async Task<bool> Commit() => await SaveChangesAsync() > 0;
}