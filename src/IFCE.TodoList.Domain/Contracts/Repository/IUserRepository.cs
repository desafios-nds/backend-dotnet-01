﻿using IFCE.TodoList.Domain.Model;

namespace IFCE.TodoList.Domain.Contracts.Repository;

public interface IUserRepository : IRepository<User>
{
    Task<User> FindByEmail(string email);
    void Add(User user);
    Task<bool> IsEmailInUse(string email);
}