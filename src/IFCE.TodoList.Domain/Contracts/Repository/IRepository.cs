﻿using IFCE.TodoList.Domain.Model;

namespace IFCE.TodoList.Domain.Contracts.Repository;

public interface IRepository<T> : IDisposable where T : Entity
{
    IUnitOfWork UnitOfWork { get; }
}